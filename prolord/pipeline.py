import json
import logging

import discord
from asgiref.sync import async_to_sync
from django.conf import settings
from social_django.models import UserSocialAuth

_logger = logging.getLogger(__name__)


@async_to_sync
async def process_member(discord_id):
    intents = discord.Intents.default()
    intents.members = True
    client = discord.Client(intents=intents)
    await client.login(settings.DISCORD_BOT_TOKEN)

    async for guild in client.fetch_guilds():
        _logger.info(f"Processing guild {guild}")
        member = await guild.fetch_member(discord_id)
        if not member:
            _logger.warn(f"User {discord_id} not found in guild {guild}")
            continue
        roles = set()
        for default_role in settings.DISCORD_DEFAULT_ROLES:
            role = list(
                filter(
                    lambda role: role.name == default_role, await guild.fetch_roles()
                )
            )
            if role:
                roles.add(role[0])
        _logger.info(
            f"Setting roles to {[role.name for role in roles]} to user_id {discord_id} in guild {guild}"
        )
        await member.add_roles(*roles)
        await member.send(
            f"Your Discord account has been linked to your Prologin Google account."
        )

    await client.close()


def add_roles_after_discord_auth(backend, user, *args, **kwargs):
    if backend.name != "discord":
        return
    if not user:
        return

    google_social_auth = UserSocialAuth.objects.get(user=user, provider="google-oauth2")
    discord_social_auth = UserSocialAuth.objects.get(user=user, provider="discord")

    _logger.info(f"Processing authentication for {user.username} {user.email}")

    discord_id = discord_social_auth.uid
    process_member(discord_id)
