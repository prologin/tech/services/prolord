from django.conf import settings
from django.contrib import admin
from django.urls import include, path


def crash(request):
    _ = 1 / 0


urlpatterns = [
    path("admin/", admin.site.urls),
    path("accounts/", include("django.contrib.auth.urls")),
    path("accounts/", include("social_django.urls", namespace="social")),
    path("api/crash/", crash),
    path("", include("bot.urls")),
]

if settings.DEBUG:
    import debug_toolbar

    urlpatterns = [path("__debug__/", include(debug_toolbar.urls))] + urlpatterns
