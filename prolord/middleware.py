from django.shortcuts import render
from social_core.exceptions import AuthAlreadyAssociated
from social_django.middleware import SocialAuthExceptionMiddleware


class AuthAlreadyAssociatedMiddleware(SocialAuthExceptionMiddleware):
    """
    Redirect users to desired-url when AuthAlreadyAssociated exception occurs.
    """

    # pylint: disable=inconsistent-return-statements
    def process_exception(self, request, exception):
        if isinstance(exception, AuthAlreadyAssociated):
            message = f"This {request.backend.name} account is already in use."
            return render(request, "base.html", {"error_message": message})
