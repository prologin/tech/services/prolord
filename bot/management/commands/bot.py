import logging

import discord
from discord.ext import commands
from django.conf import settings
from django.core.management.base import BaseCommand

_logger = logging.getLogger(__name__)


class Bot(commands.Bot):
    command_prefix = "!"

    def __init__(self, *args, **kwargs):
        intents = discord.Intents.default()
        intents.members = True
        super().__init__(
            intents=intents, command_prefix=self.command_prefix, *args, **kwargs
        )

    async def on_ready(self):
        _logger.info(f"Logged in as {self.user}")
        _logger.info(f"Connected to servers: {[(g.id, g.name) for g in self.guilds]}")

    async def on_member_join(self, member):
        await member.send(
            f"Welcome to Prologin Discord server. Please identify yourself via https://{settings.DEFAULT_DOMAIN}.",
        )


class Command(BaseCommand):
    help = "Run the Discord bot"

    def handle(self, *args, **options):
        bot = Bot()
        bot.run(settings.DISCORD_BOT_TOKEN)
