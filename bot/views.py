from django.http import HttpResponse, HttpResponseRedirect
from django.urls import reverse
from django.views.generic.base import TemplateView
from social_django.models import UserSocialAuth


class LoginView(TemplateView):
    template_name = "base.html"

    def get(self, request, *args, **kwargs):
        social_auths = UserSocialAuth.objects.filter(user_id=request.user.id)
        if not social_auths.filter(provider="google-oauth2"):
            return HttpResponseRedirect(reverse("social:begin", args=["google-oauth2"]))
        if not social_auths.filter(provider="discord"):
            return HttpResponseRedirect(reverse("social:begin", args=["discord"]))

        return super().get(request, *args, **kwargs)


class ResetView(TemplateView):
    template_name = "base.html"

    def get(self, request, *args, **kwargs):
        UserSocialAuth.objects.filter(user_id=request.user.id).delete()
        return HttpResponseRedirect(reverse("login"))
