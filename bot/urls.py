from django.urls import path

from .views import LoginView, ResetView

urlpatterns = [
    path("", LoginView.as_view(), name="login"),
    path("reset/", ResetView.as_view(), name="reset"),
]
